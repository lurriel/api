/*
  Router file.
  Extends app and gives paths to:
  GET /api/users - return all users
  POST /api/users - create a new user
  GET /api/users/:user_id - return one user based ond user ID
  PUT /api/users/:user_id - udpate a user
  DELETE /api/users/:user_id - delete a user based on user ID

  Special path to simple user UI: GET /api/ui/
*/
'use strict'

let express = require('express');
let router = express.Router();
let server = require('./../server/index');

router.route("/users")
  .get(server.getUsers)
  .post(server.postUser);
router.route("/users/:user_id")
  .get(server.getUser)
  .delete(server.deleteUser)
  .put(server.updateUser);
router.route("/ui")
  .get(server.ui);

module.exports = router;
