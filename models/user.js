/*
  MongoDB User model
  fields:
    email (string) unique field
    forename (string)
    surname (string)
    create (date)
    _id (int) unique auto-increment handle by db
*/

'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  forename: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  created: Date
});

module.exports = mongoose.model('user', userSchema);
