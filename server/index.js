/*
  This file holds functions used by server to perform API actions:

  function getUsers - GET/ - pull all users.
  function updateUser - PUT/:user_id - will udpate user based on given user ID.
                              Fields to update: email (string), forename (string) and surname (string).
  function postUser - POST/ - create new a user with required fields:
                              email (string), forename (string) and surname (string).
  function deleteUser - DELETE/:user_id - delete a user on given user ID.
  function ui - GET/ - redirects user to simple UI API page
*/
'use strict'

let mongoose = require('mongoose');
let user = require('./../models/user');
let sanitize = require('mongo-sanitize');
//GET - get all users
function getUsers(req, res) {
  let query = user.find({});
  query.exec((err, users) => {
    if (err) {
      res.send(err);
    } else {
      if (users.length === 0) {
        res.json({
          message: 'No user to display'
        });
      } else {
        res.json(users);
      }

    }
  });
}
//POST - create a new user
function postUser(req, res) {
  let newUser = new user();
  let email = sanitize(req.body.email);
  let forename = sanitize(req.body.forename);
  let surname = sanitize(req.body.surname);
  newUser.email = email;
  newUser.forename = forename;
  newUser.surname = surname;
  newUser.created = Date.now();
  newUser.save((err, newUser) => {
    if (err) {
      res.send(err);
    } else {
      res.json({
        message: 'User created!',
        newUser
      });
    }
  });
}
//GET:user_id - get user by ID
function getUser(req, res) {
  let user_id = sanitize(req.params.user_id);
  user.findById(user_id, (err, newUser) => {
    if (err) {
      res.send(err);
    } else {
      if (newUser === null) {
        res.json({
          message: 'User with given ID does not exist'
        });
      } else {
        res.json(newUser);
      }
    }
  });
}
//DELETE:user_id - delete user by ID
function deleteUser(req, res) {
  let user_id = sanitize(req.params.user_id);
  user.findById(user_id, (err, delUser) => {
    if (err) {
      res.send(err);
    } else {
      if (delUser === null) {
        res.json({
          message: 'User with given ID does not exist'
        });
      } else {
        user.remove({
          _id: user_id
        }, (err, deletedUser) => {
          if (err) {
            res.send(err);
          } else {
            res.json({
              message: 'User successfully deleted',
              deletedUser
            });
          }
        });
      }
    }
  });
}
//PUT:user_id - update user by ID
function updateUser(req, res) {
  let user_id = sanitize(req.params.user_id);
  user.findById(user_id, (err, upUser) => {
    if (err) {
      res.send(err);
    } else {
      let email, forename, surname;
      if (req.body.email !== undefined) {
        email = sanitize(req.body.email);
      } else {
        email = upUser.email;
      }
      if (req.body.forename !== undefined) {
        forename = sanitize(req.body.forename);
      } else {
        forename = upUser.forename;
      }
      if (req.body.surname !== undefined) {
        surname = sanitize(req.body.surname);
      } else {
        surname = upUser.surname;
      }
      upUser.email = email;
      upUser.forename = forename;
      upUser.surname = surname;
      upUser.save((err, updatedUser) => {
        if (err) {
          res.send(err);
        } else {
          res.json({
            message: 'User updated!',
            updatedUser
          });
        }
      });
    }
  });
}
//GET: redirect user to simple UI API page
function ui(reg, res) {
  res.render('index');
}

module.exports = {
  getUsers,
  postUser,
  getUser,
  deleteUser,
  updateUser,
  ui
};
