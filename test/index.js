'use strict';
/*
  Tests to prove that API is working
  It supports PUT/GET/POST/DELETE actions
  Based on CHAI and chaiHttp
*/

//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let user = require('./../models/user');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();

chai.use(chaiHttp);

describe('Users API TEST', () => {

  beforeEach((done) => { //Before each test we empty the database
    user.remove({}, (err) => {
      done();
    });
  });
  afterEach((done) => { //After each test we empty the database
    user.remove({}, (err) => {
      done();
    });
  });
  describe('/GET 0 users', () => {
    it('it should GET empty list of users', (done) => {
      chai.request(server)
        .get('/api/users')
        .end((err, res) => {
          res.should.have.status(200);
          res.should.have.property('body').eql({
            message: 'No user to display'
          });
          done();
        });
    });
  });
  describe('/GET users', () => {
    it('it should GET user', (done) => {
      let newUser = new user();
      newUser.email = 'testemail1@gmail.com';
      newUser.forename = 'John';
      newUser.surname = 'Smith';
      newUser.created = Date.now();
      newUser.save((err, nUser) => {
        chai.request(server)
          .get('/api/users/')
          .end((err, res) => {
            res.should.have.status(200);
            res.body[0].should.have.property('created');
            res.body[0].should.have.property('forename');
            res.body[0].should.have.property('surname');
            res.body[0].should.have.property('email');
            res.body[0].should.have.property('_id');
            done();
          });
      });
    });
  });

  describe('/POST user', () => {
    it('it should create a user', (done) => {
      let newUser = {
        email: 'testemail@gmail.com',
        forename: 'John',
        surname: 'Smith'
      }
      chai.request(server)
        .post('/api/users')
        .send(newUser)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.res.body.newUser.should.have.property('created');
          res.res.body.newUser.should.have.property('forename').eql(newUser.forename);
          res.res.body.newUser.should.have.property('surname').eql(newUser.surname);
          res.res.body.newUser.should.have.property('email').eql(newUser.email);
          done();
        });
    });
  });
  describe('/POST user without full data', () => {
    it('it should not create a user when required field is missing', (done) => {
      let newUser = {
        forename: 'John',
        surname: 'Smith'
      }
      chai.request(server)
        .post('/api/users')
        .send(newUser)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.res.body.should.have.property('errors');
          res.res.body.should.have.property('message').eql("user validation failed");
          done();
        });
    });
  });
  describe('/GET user by ID', () => {
    it('it should GET user by ID', (done) => {
      let newUser = new user();
      newUser.email = 'testemail1@gmail.com';
      newUser.forename = 'John';
      newUser.surname = 'Smith';
      newUser.created = Date.now();
      newUser.save((err, nUser) => {
        chai.request(server)
          .get('/api/users/' + nUser.id)
          .send(nUser)
          .end((err, res) => {

            res.should.have.status(200);
            res.body.should.have.property('created');
            res.body.should.have.property('forename');
            res.body.should.have.property('surname');
            res.body.should.have.property('email');
            res.body.should.have.property('_id').eql(nUser.id);
            done();
          });
      });
    });
  });
  describe('/PUT/:id user', () => {
    it('it should UPDATE a user based on given id', (done) => {
      let updateUser = {
        email: 'newMail@gmail.com',
        forename: 'Tony',
        surname: 'Adams'
      }
      let newUser = new user();
      newUser.email = 'testemail@gmail.com';
      newUser.forename = 'John';
      newUser.surname = 'Smith';
      newUser.created = Date.now();
      newUser.save((err, nUser) => {
        chai.request(server)
          .put('/api/users/' + nUser.id)
          .send({
            email: updateUser.email,
            forename: updateUser.forename,
            surname: updateUser.surname
          })
          .end((err, res) => {

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.updatedUser.should.have.property('forename').eql(updateUser.forename);
            res.body.updatedUser.should.have.property('surname').eql(updateUser.surname);
            res.body.updatedUser.should.have.property('email').eql(updateUser.email);
            done();
          });
      });
    });
  });
  describe('/DELETE/:id user', () => {
    it('it should DELETE a user', (done) => {
      let newUser = new user();
      newUser.email = 'testemail@gmail.com';
      newUser.forename = 'John';
      newUser.surname = 'Smith';
      newUser.created = Date.now();
      newUser.save((err, nUser) => {
        chai.request(server)
          .delete('/api/users/' + newUser.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('User successfully deleted');
            res.body.deletedUser.should.have.property('ok').eql(1);
            res.body.deletedUser.should.have.property('n').eql(1);
            done();
          });
      });
    });
  });
});
