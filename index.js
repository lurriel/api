/*
  Main file. Sets whole application to run.
*/
'use strict';
let compression = require('compression')
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let router = require('./routes/');
let mongoose = require('mongoose');
let port = process.env.PORT || 3000;
let morgan = require('morgan');
let config = require('config'); //Load the db location from the JSON files
let hbs = require('hbs');
//Compression
function shouldCompress(req, res) {
  return true;
}
//Compression
app.use(compression({ threshold: 0, filter: shouldCompress }));

app.set('view engine', 'html');
app.engine('html', hbs.__express);

let options = {
  server: {
    socketOptions: {
      keepAlive: 1,
      connectTimeoutMS: 30000
    }
  },
  replset: {
    socketOptions: {
      keepAlive: 1,
      connectTimeoutMS: 30000
    }
  }
};
mongoose.connect(config.DBHost, options);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

//Hide log when it is a test
if (config.util.getEnv('NODE_ENV') !== 'test') {
  app.use(morgan('combined'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.text());
app.use(bodyParser.json({
  type: 'application/json'
}));

app.use('/api', router);

app.listen(port);
console.log('Server running on port %d', port);

module.exports = app;
